#!/bin/bash
#set -x

Usage() {
    cat <<'END_OF_USAGE'
    Usage:
        sh sync_build.sh [options]
        Example:  sh sync_build.sh -b LE.UM.6.4.2 -d -c
        Options:
            -h|--help : help
            -b|--branch : provide branch name [Mandatory]
            -c|--compile : Compile the code [Optional]
            -d|--download : Download code only [Mandatory]

END_OF_USAGE

    exit 1

}

## Analyze yaml
function parse_yaml {
    prefix=$2
    s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
    sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |

    awk -F$fs '{
        indent = length($1)/2;
        vname[indent] = $2;
        for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
            }
        }'
}

clone_from_chipcode(){

    echo "Start to clone chipcode"
    set -x
    pushd $chipcode_downloads_path
    git config --global http.'https://chipmaster2.qti.qualcomm.com'.followRedirects "true"
    git clone -b $release_version --depth 1 $chipcode_download_url $chipcode_downloads_path
    popd
    set +x

}

sync_baseau_source_code(){

    echo "sync base AU oss source code from codelinaro"
    pushd $baseau_workspace
    cp -rn $chipcode_downloads_path/apps_proc/* $baseau_workspace/

    echo "<?xml version='1.0' encoding='UTF-8'?>
    <snap_release>
            <image oss_url=\"https://git.codelinaro.org\" image_type=\"TBD\" si_chipcode_path=\"$chipcode_downloads_path/apps_proc\" prebuilts_dir=\"product_prebuilt_dir\" oss_manifest_git=\"clo/le/le/product/manifest\" hf_manifest_branch=\"\" hf_manifest_git=\"\" au_tag=\"$baseAU_manifest\" url=\"https://chipcode.qti.qualcomm.com\" software_image=\"LE.UM.6.4.2\"/>
            <image oss_url=\"https://git.codelinaro.org\" image_type=\"TBD\" si_chipcode_path=\"$chipcode_downloads_path/ros_proc\" prebuilts_dir=\"ros_prebuilt_dir\" oss_manifest_git=\"clo/le/sdkrobotics/ros/manifest\" hf_manifest_branch=\"\" hf_manifest_git=\"\" au_tag=\"$ros_manifest\" url=\"https://chipcode.qti.qualcomm.com\" software_image=\"ROS.FOXY.1.0\"/>
           <image image_type=\"TBD\" all=\"LE.UM.6.4.2 ROS.FOXY.1.0\"/>
    </snap_release>" > ./snap_release_esdk.xml

    chmod 755 ./snap_release_esdk.xml
    set -x
    ./sync_snap_v2.sh --workspace_path=$baseau_workspace --snap_release=$baseau_workspace/snap_release_esdk.xml --tree_type=all --opt=chipcode --repo_url=https://git.codelinaro.org/clo/tools/repo.git --repo_branch=aosp-new/stable  --shallow_clone=true
    if [ "$?" != "0" ]; then
        echo "sync fail"
        exit 1
    fi
    popd
    set +x

}

build_baseau_image() {

    echo "build base AU image"
    pushd $baseau_workspace
    set -x
    MACHINE=qrb5165-rb5 DISTRO=qti-distro-fullstack-$build_varaint source poky/qti-conf/set_bb_env.sh
    export PREBUILT_SRC_DIR=$baseau_workspace/prebuilt_HY11
    bitbake qti-robotics-image
    if [ "$?" != "0" ]; then
        echo "build base AU image failed"
        exit 1
    fi
    popd
    set +x

}

generate_esdk() {

    echo "Generate eSDK "
    set -x
    pushd $baseau_workspace
    MACHINE=qrb5165-rb5 DISTRO=qti-distro-fullstack-$build_varaint source poky/qti-conf/set_bb_env.sh
    export PREBUILT_SRC_DIR=$baseau_workspace/prebuilt_HY11
    bitbake qti-robotics-image -c populate_sdk_ext
    popd
    set +x

}

sync_build_baseau() {

    if [ "$download" == "1" ]; then
        echo "let sync baseAU!"
        sync_baseau_source_code
      else
        echo "Skip sync base AU"
    fi

    if [ "$compile" == "1" ]; then
        echo "let build baseAU!"
        build_baseau_image
        echo "build baseau image successfully"
        generate_esdk
        echo "eSDK is generated successfully!"
        echo "eSDK path: $baseau_workspace/build-qti-distro-fullstack-$build_varaint/tmp-glibc/deploy/sdk/"
      else
        echo "Skip build base AU"
    fi

}

sync_qirf_oss_source_code(){

    echo "sync function sdk oss source code from codelinaro"
    set -x
    pushd $qirf_workspace
    repo init -u https://git.codelinaro.org/clo/le/sdkrobotics/qirf/manifest.git -b release -m $qirf_manifest.xml
    repo sync -c -j8 -q
    if [ "$?" != "0" ]; then
      echo "sync QIRF fail"
      exit 1
    fi
    popd
    set +x

}

combine_qirf_code(){

    echo "copy chipcode to qirf"
    cp -rn $chipcode_downloads_path/qirf_proc/apps_proc/* $qirf_workspace/
    if [ "$?" != "0" ]; then
      echo "copy chipcode to qirf failed"
      exit 1
    fi

}

prepare_qirf_dependency_artifacts(){

    echo "copy qim artifacts to qirf"
    mkdir -p $qirf_workspace/function_sdk/qim_sdk
    cp -rn $qim_workspace/* $qirf_workspace/function_sdk/qim_sdk/

    if [ "$?" != "0" ]; then
      echo "copy qim artifacts to qirf failed"
      exit 1
    fi

}

install_esdk_to_qirf(){

    echo "copy eSDK to qirf"
    set -x
    pushd $qirf_workspace
    umask 022
    $baseau_workspace/build-qti-distro-fullstack-$build_varaint/tmp-glibc/deploy/sdk/fullstack-*.sh -y -d $qirf_workspace

    if [ "$?" != "0" ]; then
      echo "build image fail"
      exit 1
    fi
    popd
    set +x

}

compile_qirf(){

    echo "start compile qirf"
    set -x
    pushd $qirf_workspace
    . environment-setup-aarch64-oe-linux
    devtool build-image qti-robotics-image

    if [ "$?" != "0" ]; then
      echo "compile qirf image fail"
      exit 1
    fi
    popd
    set +x

}

sync_build_qirf() {

    if [ "$download" == "1" ]; then
        echo "let sync QIRF.SDK.1.0!"
        sync_qirf_oss_source_code
        combine_qirf_code
        prepare_qirf_dependency_artifacts
      else
        echo "Skip sync base AU"
    fi

    if [ "$compile" == "1" ]; then
        echo "sync and build BASE AU firstly"
        if [ ! -d $baseau_workspace/build-qti-distro-fullstack-$build_varaint/tmp-glibc/deploy/sdk ];then
            sync_build_baseau
            else
            echo "The eSDK has been generated!"
        fi
        echo "install esdk to qirf"
        install_esdk_to_qirf
        echo "build QIRF"
        compile_qirf
        echo "qirf compile successfully!"
        echo "qirf artifacts: $qirf_workspace/tmp/deploy/artifacts/"
      else
        echo "Skip build QIRF"
    fi

}

sync_qirp_oss_source_code(){

    echo "sync product sdk oss source code from codelinaro"
    set -x
    pushd $qirp_workspace
    repo init -u https://git.codelinaro.org/clo/le/sdkrobotics/qirp/manifest.git -b release -m $qirp_manifest.xml

    repo sync -c -j8 -q

    if [ "$?" != "0" ]; then
      echo "sync QIRP oss source code fail"
      exit 1
    fi
    popd
    set +x

}

combine_qirp_code(){

    echo "copy chipcode to qirp"
    cp -rn $chipcode_downloads_path/qirp_proc/apps_proc/* $qirp_workspace/

    if [ "$?" != "0" ]; then
      echo "copy chipcode to qirp failed"
      exit 1
    fi

}

prepare_qirp_dependency_artifacts(){

    echo "copy qim artifacts"
    mkdir -p $qirp_workspace/function_sdk/qim_sdk
    cp -rn $qim_workspace/* $qirp_workspace/function_sdk/qim_sdk/

    echo "copy QIRF artifacts"
    mkdir -p $qirp_workspace/function_sdk/qirf_sdk
    cp -rn $qirf_workspace/tmp/deploy/artifacts/* $qirp_workspace/function_sdk/qirf_sdk/

    echo "copy snpe artifacts"
    mkdir -p $qirp_workspace/function_sdk/snpe_sdk
    cp -rn $snpe_workspace/* $qirp_workspace/function_sdk/snpe_sdk/

    if [ "$?" != "0" ]; then
      echo "prepare dependency artifacts fail"
      exit 1
    fi

}

setup_esdk() {

    echo "copy eSDK"
    set -x
    pushd $qirp_workspace
    umask 022

    $baseau_workspace/build-qti-distro-fullstack-$build_varaint/tmp-glibc/deploy/sdk/fullstack-*.sh -y -d $qirp_workspace
    if [ "$?" != "0" ]; then
      echo "setup_esdk fail"
      exit 1
    fi
    popd
    set +x

}

compile_qirp(){

    echo "start compile qirp"
    set -x
    pushd $qirp_workspace
      . environment-setup-aarch64-oe-linux
      devtool build-image qti-robotics-image
    if [ "$?" != "0" ]; then
      echo "compile qirp fail"
      exit 1
    fi
    popd
    set +x

}

sync_build_qirp(){

    if [ "$download" == "1" ]; then
        echo "let sync QIRF.SDK.1.0!"
        sync_qirp_oss_source_code
        combine_qirp_code
      else
        echo "Skip sync base AU"
    fi

    if [ "$compile" == "1" ]; then
        echo "sync and build BASE AU firstly"
        if [ ! -d $baseau_workspace/build-qti-distro-fullstack-$build_varaint/tmp-glibc/deploy/sdk ];then
            sync_build_baseau
            else
            echo "The eSDK has been generated!"
        fi
        if [ ! -d $qirf_workspace/tmp/deploy/artifacts/ ];then
            sync_build_qirf
            else
            echo "The qirf artifacts has been generated!"
        fi
        echo "Prepare dependent artifacts"
        prepare_qirp_dependency_artifacts
        echo "install esdk"
        setup_esdk
        echo "build QIRP"
        compile_qirp
        echo "qirp compile successfully!"
        echo "qirp artifacts: $qirp_workspace/tmp/deploy/artifacts/"
      else
        echo "Skip build QIRP"
    fi

}

sync_full_image_source_code(){

    echo "sync base AU and QIRP oss source code from codelinaro"
    pushd $fullimage_workspace
    cp -rn $chipcode_downloads_path/apps_proc/sync_snap_v2.sh $fullimage_workspace/

    echo "<?xml version='1.0' encoding='UTF-8'?>
    <snap_release>
            <image oss_url=\"https://git.codelinaro.org\" image_type=\"TBD\" si_chipcode_path=\"$chipcode_downloads_path/qirp_proc\" prebuilts_dir=\"qirp_prebuilt_dir\" oss_manifest_git=\"clo/le/sdkrobotics/qirp/manifest\" hf_manifest_branch=\"\" hf_manifest_git=\"\" au_tag=\"$qirp_manifest\" url=\"https://chipcode.qti.qualcomm.com\" software_image=\"QIRP.SDK.1.0\"/>
            <image oss_url=\"https://git.codelinaro.org\" image_type=\"TBD\" si_chipcode_path=\"$chipcode_downloads_path/apps_proc\" prebuilts_dir=\"product_prebuilt_dir\" oss_manifest_git=\"clo/le/le/product/manifest\" hf_manifest_branch=\"\" hf_manifest_git=\"\" au_tag=\"$baseAU_manifest\" url=\"https://chipcode.qti.qualcomm.com\" software_image=\"LE.UM.6.4.2\"/>
            <image oss_url=\"https://git.codelinaro.org\" image_type=\"TBD\" si_chipcode_path=\"$chipcode_downloads_path/ros_proc\" prebuilts_dir=\"ros_prebuilt_dir\" oss_manifest_git=\"clo/le/sdkrobotics/ros/manifest\" hf_manifest_branch=\"\" hf_manifest_git=\"\" au_tag=\"$ros_manifest\" url=\"https://chipcode.qti.qualcomm.com\" software_image=\"ROS.FOXY.1.0\"/>
            <image image_type=\"TBD\" all=\"QIRP.SDK.1.0 LE.UM.6.4.2 ROS.FOXY.1.0\"/>
    </snap_release>" > ./snap_release_prebuilt.xml

    chmod 755 ./snap_release_prebuilt.xml
    set -x
    ./sync_snap_v2.sh --workspace_path=$fullimage_workspace --snap_release=$fullimage_workspace/snap_release_prebuilt.xml --tree_type=all --opt=chipcode --repo_url=https://git.codelinaro.org/clo/tools/repo.git --repo_branch=aosp-new/stable  --shallow_clone=true

    cp -rn $fullimage_workspace/apps_proc/* $fullimage_workspace/
    if [ "$?" != "0" ]; then
        echo "build fail"
        exit 1
    fi
    popd
    set +x

}

prepare_full_image_dependency_artifacts(){

    echo "copy qim artifacts to fullimage workspace"
    mkdir -p $fullimage_workspace/function_sdk/qim_sdk
    cp -rn $qim_workspace/* $fullimage_workspace/function_sdk/qim_sdk/

    echo "copy QIRF artifacts to fullimage workspace"
    mkdir -p $fullimage_workspace/function_sdk/qirf_sdk
    cp -rn $qirf_workspace/tmp/deploy/artifacts/*  $fullimage_workspace/function_sdk/qirf_sdk/

    echo "copy snpe artifacts to fullimage workspace"
    mkdir -p $fullimage_workspace/function_sdk/snpe_sdk
    cp -rn $snpe_workspace/* $fullimage_workspace/function_sdk/snpe_sdk/

    if [ "$?" != "0" ]; then
      echo "prepare dependency artifacts fail"
    exit 1
    fi

}

build_full_image() {

    echo "build full image"
    set -x
    pushd $fullimage_workspace
    MACHINE=qrb5165-rb5 DISTRO=qti-distro-fullstack-debug source poky/qti-conf/set_bb_env.sh
    cp -rn $fullimage_workspace/function_sdk ./
    export PREBUILT_SRC_DIR=$fullimage_workspace/prebuilt_HY11
    bitbake qti-robotics-image
    if [ "$?" != "0" ]; then
        echo "build full image fail"
        exit 1
    fi
    popd
    set +x

}

sync_build_full_image() {

    sync_full_image_source_code
    prepare_full_image_dependency_artifacts
    build_full_image

}

###################################################
######### script start from here ##################
###################################################


##Get the parameters
LONG_OPTS="help:,branch:,compile::,download::,"
GETOPT_CMD=$(getopt -o h:b:c::d:: --long $LONG_OPTS \
                    -n $(basename $0) -- "$@") ||  { echo "Getopt failed"; Usage; }

eval set -- "$GETOPT_CMD"

variant="None"

while true ; do
    case "$1" in
        -h|--help) Usage;;
        -b|--branch) branch="$2"; shift;;
        -c|--compile) compile=1; shift;;
        -d|--download) download=1; shift;;
        --) shift; break;;
    esac
    shift
done

echo "branch:$branch"
echo "download:$download"
echo "compile:$compile"

if [ "$branch" == "LE.UM.6.4.2" ] || [ "$branch" == "QIRP.SDK.1.0" ] || [ "$branch" == "QIRF.SDK.1.0" ] || [ "$branch" == "QIM.SDK.1.0" ]; then
    echo "branch:$branch , let start sync and build "
    else
    echo "Note: -b(branch,It has to be one or the other) : LE.UM.6.4.2/QIRF.SDK.1.0/QIRP.SDK.1.0/QIM.SDK.1.0"
    Usage
    exit 1
fi

eval $(parse_yaml ./sync_build.yaml)

echo "chipcode_downloads_path:$chipcode_downloads_path"
echo "baseau_workspace:$baseau_workspace"
echo "qirf_workspace:$qirf_workspace"
echo "qirp_workspace:$qirp_workspace"
echo "qim_workspace:$qim_workspace"
echo "snpe_workspace:$snpe_workspace"

echo "release_version:$release_version"
echo "chipcode_download_url:$chipcode_download_url"

echo "baseAU_manifest:$baseAU_manifest"
echo "qirf_manifest:$qirf_manifest"
echo "qirp_manifest:$qirp_manifest"
echo "qim_manifest:$qim_manifest"
echo "ros_manifest:$ros_manifest"

echo "build_varaint:$build_varaint"

## Prepare the code directory

mkdir -p $chipcode_downloads_path
mkdir -p $baseau_workspace
mkdir -p $qirf_workspace
mkdir -p $qirp_workspace
mkdir -p $qim_workspace
mkdir -p $snpe_workspace

if [ ! -d $chipcode_downloads_path/apps_proc ];then
      clone_from_chipcode
    else
      echo "chipcdoe has downloaded"
fi

if [ "$branch" == "LE.UM.6.4.2" ]; then

    sync_build_baseau

fi

if [ "$branch" == "QIRF.SDK.1.0" ]; then

    sync_build_qirf

fi

if [ "$branch" == "QIRP.SDK.1.0" ]; then

    sync_build_qirp

fi

if [ "$branch" == "fullimage" ]; then

    sync_build_full_image

fi
