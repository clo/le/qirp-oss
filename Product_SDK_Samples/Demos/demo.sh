#Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

#!/bin/bash

mount -o remount,rw /

if [ ! -d /data/output ]
then
    mkdir /data/output
fi

if [ ! -d /vendor/etc/camera ]
then
    mkdir /vendor/etc/camera
fi

echo "run demo.sh"

Main_Camera_Snapshot_4K60_Waylandsink_Display()
{

echo enableNCSService=FALSE >> /vendor/etc/camera/camxoverridesettings.txt
echo HALOutputBufferCombined=FALSE >> /vendor/etc/camera/camxoverridesettings.txt
export XDG_RUNTIME_DIR=/run/user/root
export WAYLAND_DISPLAY=wayland-1

gst-pipeline-app -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 ! waylandsink sync=true fullscreen=true camsrc. ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 ! queue ! qtic2venc ! queue ! h265parse ! mp4mux ! queue ! filesink location=/data/out/vid.mp4 camsrc.image_2 ! "image/jpeg,width=3840,height=2160" ! multifilesink location=/data/frame%d.jpg sync=true async=false
}


Main_Camera_Encode_4K60()
{

gst-launch-1.0 -e qtiqmmfsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=30/1 ! qtic2venc min-quant-i-frames=20 min-quant-p-frames=20 max-quant-i-frames=30 max-quant-p-frames=30 quant-i-frames=20 quant-p-frames=20 ! queue ! h264parse ! mp4mux ! queue ! filesink location="/data/output/mux4K.mp4"
}

Video_Decode_4K60_Waylandsink_Display()
{
echo "please check if there is an file /data/output/mux4K.mp4 !"
echo "You can push an same name file to the path or run Main Camera Encode_4K60 demo first"
export GST_PLUGIN_PATH=/usr/lib/gstreamer-1.0 && export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 filesrc location=/data/output/mux4K.mp4 ! qtdemux ! queue ! h264parse ! qtic2vdec ! queue ! waylandsink sync=false fullscreen=true
}

Main_Camera_Preview_4K60_Waylandsink_Display()
{
echo "run demo commond line is : gst-launch-1.0 -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 !  waylandsink fullscreen=true async=true sync=false"

export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 !  waylandsink fullscreen=true async=true sync=false
}

Audio_PCM_Record()
{
echo "run demo command is "export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -v pulsesrc ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! audioconvert ! wavenc ! filesink location=/data/output/track0.wav""

export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -v pulsesrc ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! audioconvert ! wavenc ! filesink location=/data/output/track0.wav
}

Audio_PCM_Playback()
{
echo "please check if there is an file /data/output/track0.wav !"
echo "You can push an same name file to the path or run Audio PCM Record demo first"
echo "run command is "gst-launch-1.0 filesrc location=/data/output/track0.wav ! wavparse ! audioconvert ! pulsesink""
gst-launch-1.0 filesrc location=/data/output/track0.wav ! wavparse ! audioconvert ! pulsesink
}

Weston_simple_egl()
{
echo "run command is "export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && weston-simple-egl""
export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && weston-simple-egl
}


Weston_simple_shm()
{
echo "run command is "export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && weston-simple-shm""
export WAYLAND_DISPLAY=wayland-1 && export XDG_RUNTIME_DIR=/run/user/root && weston-simple-shm
}


if [[ $1 == "Main_Camera_Preview_4K60_Waylandsink_Display" ]];then
    Main_Camera_Preview_4K60_Waylandsink_Display
elif [[ $1 == "Main_Camera_Snapshot_4K60_Waylandsink_Display" ]];then
    Main_Camera_Snapshot_4K60_Waylandsink_Display
elif [[ $1 == "Main_Camera_Encode_4K60" ]];then
    Main_Camera_Encode_4K60
elif [[ $1 == "Video_Decode_4K60_Waylandsink_Display"  ]];then
    Video_Decode_4K60_Waylandsink_Display
elif [[ $1 == "Audio_PCM_Record"   ]];then
    Audio_PCM_Record
elif [[ $1 == "Audio_PCM_Playback"   ]];then
    Audio_PCM_Playback
elif [[ $1 == "Weston_simple_egl"   ]];then
    Weston_simple_egl
elif [[ $1 == "Weston_simple_shm"   ]];then
    Weston_simple_shm
else
    echo "please run again and enter demo name : "
    echo "Weston_simple_egl"
    echo "Weston_simple_shm"
    echo "Audio_PCM_Record"
    echo "Audio_PCM_Playback"
    echo "Main_Camera_Snapshot_4K60_Waylandsink_Display"
    echo "Main_Camera_Preview_4K60_Waylandsink_Display"
    echo "Main_Camera_Encode_4K60"
    echo "Video_Decode_4K60_Waylandsink_Display"
fi

