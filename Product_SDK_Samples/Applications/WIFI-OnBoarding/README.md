# WIFI-On-Boarding
## Overview
WIFI-On-Boarding mainly includes scanning the surrounding hotspots in STA mode, creating a new WIFI connection, and obtaining the ssid and psk of the currently connected WIFI. After switching the SAP mode, you can create a new hotspot and get the ssid and psk of the current hotspot.

## 1. Init:
Install QIRP SDK and source the environment on PC
Download sample code on PC

## 2. Compile the demo app
```
$ cd WIFI-OnBoarding/wifi
$ $CC wifi.c -o wifi
```

## 3. Push the binary to the device and run
```
$ adb disable-verity
$ adb reboot
$ adb wait-for-device root
### The above three steps only need to be operated once and will always be valid.

$ adb shell mount -o remount,rw /
$ adb push wifi /data
$ adb shell
$ cd /data
$ chmod 777 wifi
$ mkdir -p /etc/wlan/sockets
$ ./wifi
```

## License
This is licensed under the BSD 3-Clause-Clear “New” or “Revised” License. Check out the [LICENSE](LICENSE) for more details.
