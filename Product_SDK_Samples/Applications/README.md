![Qualcomm Innovation Center, Inc.](Docs/images/logo-quic-on@h68.png)

This is a repositary of all samples application that can run with QIRP SDK on device. Each sample application has its own folder. An introduction for every sample application is given below. Please follow the README in the respective folders to build, deploy and test. 
Attention: Please use POST-CS version meta and with rb-debug HLOS version to use sample-apps.


## 1. Codec2-Video
```
This app aims to help users to learn how to encode with h264 and decode the video with h264 on the Qualcom platform through this sample app.
```

## 2.Device-Information(Device-info)
```
A command based App to get platform information.
from varying target types (GPUs, CPUs, USB, display, sensors and so on).
```

## 3. Device-Platform(GPIO-samples)
```
A commands based app to get and set GPIO.
For control LED, set GPIO output, Button even catching, etc.
```
## 4. GstAlsaPlugin-Application
```
Based on gstreamer. The purpose is helping users to learn how to implement the playback and recording functions of gstreamer+alsa
on the QTI platform through this sample app.
```

## 5. Gstreamer-Applications
```
The purpose of these samples is helping users to learn how to implement the functions of gstreamer on the Qualcomm platform.
```

## 6. Gstreamer-TFlite
```
Gstreamer-TFlite is to show the label of object with TFlite using Gstreamer commands.
```

## 7. OpenCL-Application
```
OpenCL-Sample-code shows OpenCL from three examples.
"FFT" shows the use of OpenCL for fast Fourier transform;
"Benchmark" shows the reading and writing rate when the memory unit is respectively Byte, KB, MB;
"Matrix_multiply" shows the multiplication of two 20*20 matrices,And print out the results of the two input matrices and the multiplication of the two matrices on the screen
```

## 8. WIFI-On-Boarding
```
WIFI-On-Boarding mainly includes scanning the surrounding hotspots in STA mode, creating a new WIFI connection,
and obtaining the ssid and psk of the currently connected WIFI. After switching the SAP mode, you can create a
newhotspot and get the ssid and psk of the current hotspot.
```

## 9. Weston-Client-Application
```
Weston-Sample-code shows how to compile and run a simple client in Weston.
```

## 10. Camera-HAL3-Sample
```
The Camera-HAL3-Sample demo calls the camera through the Camera HAL API to complete the preview capture and video operations.
```

## 11. Snpe_classification
```
Classification is to show the label name of the object with Snpe using Gstreamer commands.
```

## 12. Snpe_detection
```
Detection is to frame the object and show the name of the object with Snpe using Gstreamer commands.
```

## 13. TFlite_classification
```
Classification is to show the label name of the object with TFlite using Gstreamer commands.
```

## 14. TFLite_Posenet
```
Posenet is to show the effect of pose recognition using Gstreamer commands.
```

## 15. TFLite_Segmentation
```
Segmentation is to show the effect of object segmentation using Gstreamer commands.
```


## Contributions
Please read our [Contributions Page](CONTRIBUTING.md) for more information on contributing features or bug fixes. We look forward to your participation!

## Team
A community-driven project maintained by Qualcomm Innovation Center, Inc.

## License
Sample applications here are licensed under the BSD 3-clause-Clear “New” or “Revised” License. Check out the [LICENSE](LICENSE) for more details.
